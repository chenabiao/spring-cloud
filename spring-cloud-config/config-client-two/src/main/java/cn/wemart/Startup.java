package cn.wemart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by chenbiao on 17-8-1.
 */
@SpringBootApplication
@EnableEurekaClient
@RestController
@RefreshScope
public class Startup {

    public static void main(String[] args) {
        SpringApplication.run(Startup.class, args);
    }

    @Value("${jdbc.url}")
    String url;

    @Value("${jdbc.url1}")
    String url1;

    @Value("${http.socketTimeout}")
    String socketTimeout;

    @Value("${rabbitmq.userName}")
    String rabbitmqUserName;

    @RequestMapping(value = "/hi")
    public String hi() {
        return "jdbc.url : " + url + "  jdbc.url1 : " + url1 + "  http.socketTimeout : " + socketTimeout + "  rabbitmq.userName : " + rabbitmqUserName;
    }
}
