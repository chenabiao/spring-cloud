package cn.wemart.amqp.rabbitmq.ctxinit;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.config.StatelessRetryOperationsInterceptorFactoryBean;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.rabbit.transaction.RabbitTransactionManager;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.config.TaskExecutorFactoryBean;

/**
 * Created by chenbiao on 17-6-22.
 */
@Configuration
public class RabbitMqInit {

    @Value("${rabbitmq.dragonsoft.productsync.host}")
    private String HOST;
    @Value("${rabbitmq.dragonsoft.productsync.userName}")
    private String USER_NAME;
    @Value("${rabbitmq.dragonsoft.productsync.virtualHost}")
    private String VIRTUAL_HOST;
    @Value("${rabbitmq.dragonsoft.productsync.password}")
    private String PASSWORD;
    @Value("${rabbitmq.dragonsoft.productsync.poolsize}")
    private int POOL_SIZE;
    @Value("${rabbitmq.dragonsoft.productsync.connection.timeout}")
    private int TIME_OUT;
    @Value("${rabbitmq.dragonsoft.productsync.port}")
    private int PORT;
    @Value("${rabbitmq.dragonsoft.productsync.prefetchCount}")
    private int  PREFETCH_COUNT;
    @Value("${rabbitmq.dragonsoft.productsync.concurrentConsumers}")
    private int CONCURRENT_CONSUMERS;
    @Value("${rabbitmq.dragonsoft.productsync.maxConcurrentConsumers}")
    private int MAX_CONCURRENT_CONSUMERS;
    @Value("${rabbitmq.dragonsoft.productsync.channelTransacted}")
    private boolean CHANNEL_TRANSACTED;
    @Value("${rabbitmq.dragonsoft.productsync.maxAttempts}")
    private int MAX_ATTEMPTS;
    @Value("${rabbitmq.dragonsoft.productsync.initialInterval}")
    private Long INITIAL_INTERVAL;
    @Value("${rabbitmq.dragonsoft.productsync.maxInterval}")
    private Long MAX_INTERVAL;
    @Value("${rabbitmq.dragonsoft.productsync.multiplier}")
    private Double MULTIPLIER;
    @Value("${rabbitmq.dragonsoft.productsync.executorQueueCapacity}")
    private int EXECUTOR_QUEUE_CAPACITY;
    @Value("${rabbitmq.dragonsoft.productsync.executorPoolSize}")
    private String EXECUTOR_POOL_SIZE;

    @Bean
    CachingConnectionFactory connectionFactory(){
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(HOST);
        connectionFactory.setUsername(USER_NAME);
        connectionFactory.setPassword(PASSWORD);
        connectionFactory.setPort(PORT);
        connectionFactory.setVirtualHost(VIRTUAL_HOST);
        connectionFactory.setConnectionTimeout(TIME_OUT);
        connectionFactory.setChannelCacheSize(POOL_SIZE);
        return connectionFactory;
    }

    @Bean
    RabbitAdmin rabbitAdmin(){
        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory());
        return rabbitAdmin;
     }

     @Bean
     AmqpTemplate amqpTemplate(){
         RabbitTemplate template = new RabbitTemplate(connectionFactory());
         template.setReplyTimeout(1000);
         template.setMessageConverter(new Jackson2JsonMessageConverter());
         return template;
     }

    @Bean
    TaskExecutorFactoryBean taskExecutorFactoryBean(){
        TaskExecutorFactoryBean taskExecutorFactoryBean = new TaskExecutorFactoryBean();
        taskExecutorFactoryBean.setPoolSize(EXECUTOR_POOL_SIZE);
        taskExecutorFactoryBean.setQueueCapacity(EXECUTOR_QUEUE_CAPACITY);
        return taskExecutorFactoryBean;
    }

    @Bean
    @Qualifier(value="rabbitTx")
    RabbitTransactionManager rabbitTransactionManager(){
        RabbitTransactionManager rabbitTransactionManager = new RabbitTransactionManager();
        rabbitTransactionManager.setConnectionFactory(connectionFactory());
        return rabbitTransactionManager;
    }

    @Bean
    ExponentialBackOffPolicy  exponentialBackOffPolicy(){
        ExponentialBackOffPolicy   exponentialBackOffPolicy = new ExponentialBackOffPolicy();
        exponentialBackOffPolicy.setInitialInterval(INITIAL_INTERVAL);
        exponentialBackOffPolicy.setMaxInterval(MAX_INTERVAL);
        exponentialBackOffPolicy.setMultiplier(MULTIPLIER);
        return exponentialBackOffPolicy;
    }

    @Bean
    SimpleRetryPolicy simpleRetryPolicy(){
        SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy();
        simpleRetryPolicy.setMaxAttempts(MAX_ATTEMPTS);
        return simpleRetryPolicy;
    }

    @Bean
    RetryTemplate retryTemplate(){
        RetryTemplate retryTemplate = new RetryTemplate();
        retryTemplate.setBackOffPolicy(exponentialBackOffPolicy());
        retryTemplate.setRetryPolicy(simpleRetryPolicy());
        return retryTemplate;
    }

    @Bean
    StatelessRetryOperationsInterceptorFactoryBean retryInterceptor(){
        StatelessRetryOperationsInterceptorFactoryBean retryInterceptor = new StatelessRetryOperationsInterceptorFactoryBean();
        retryInterceptor.setMessageRecoverer(new RejectAndDontRequeueRecoverer());
        retryInterceptor.setRetryOperations(retryTemplate());
        return retryInterceptor;
    }

    @Bean
    SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory (){
        SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory = new SimpleRabbitListenerContainerFactory();
        rabbitListenerContainerFactory.setConnectionFactory(connectionFactory());
        /*prefetchCount设置每个消费者平均分配的个数,即通知RabbitMQ不要在同一时间给一个消费者超过一条消息.*/
        rabbitListenerContainerFactory.setPrefetchCount(PREFETCH_COUNT);
       /* rabbitConnFactory的channel-cache-size必须大于concurrentConsumers,taskExecutor的pool-size最小值要大于concurrentConsumers*/
        rabbitListenerContainerFactory.setConcurrentConsumers(CONCURRENT_CONSUMERS);
        rabbitListenerContainerFactory.setMaxConcurrentConsumers(MAX_CONCURRENT_CONSUMERS);
        rabbitListenerContainerFactory.setChannelTransacted(CHANNEL_TRANSACTED);
        rabbitListenerContainerFactory.setAdviceChain(retryInterceptor().getObject());
        rabbitListenerContainerFactory.setTransactionManager(rabbitTransactionManager());
        rabbitListenerContainerFactory.setTransactionManager(rabbitTransactionManager());
        return rabbitListenerContainerFactory;
    }

}
