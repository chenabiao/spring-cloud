package cn.wemart.amqp.rabbitmq.core.init;

import cn.wemart.amqp.rabbitmq.core.domain.data.DataExchange;
import cn.wemart.amqp.rabbitmq.core.dao.MqQueueMapper;
import cn.wemart.amqp.rabbitmq.core.domain.para.ParaConsumerBean;
import cn.wemart.amqp.rabbitmq.mgnt.RabbitMqAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by huangkai on 17-6-20.
 */

/**
 *更新模块暂时用不到
 * 17-6-26
 */
@Component
public class UpdateMessageDefine {

    private final static Logger logger = LoggerFactory.getLogger(CreateMessageDefine.class);
    private List<DataExchange> listDataExchange;


    @Autowired
    private MqQueueMapper mq;

    @Autowired
    private RabbitMqAdmin rabbitMqCli;

    public int updateMessage(ParaConsumerBean paraConsumerBean){

        logger.info("updating message");

        listDataExchange=mq.selectAllMq();
        if(listDataExchange.size()==0||listDataExchange==null){
            logger.warn("DataExchange获取失败");
            return 0;
        }


        try{
            for(DataExchange dataExchange:listDataExchange){

                if (dataExchange.getType() == 1) {
                    rabbitMqCli.deleteExchange(dataExchange.getExchangeName());
                    rabbitMqCli.deleteExchange("dead.exchange." + dataExchange.getQueueName());
                }
                rabbitMqCli.deleteQueue(dataExchange.getQueueName(),paraConsumerBean.isUnUseFlag(),paraConsumerBean.isEmptyFlag());
                rabbitMqCli.deleteQueue("dead." + dataExchange.getQueueName(),paraConsumerBean.isUnUseFlag(),paraConsumerBean.isEmptyFlag());

            }


        }catch (Exception e){
            logger.error("UpdateMessageDefine失败");
            return 0;
        }

        return 1;
    }

    public int initRabbitMq(ParaConsumerBean paraConsumerBean){

        boolean f1=rabbitMqCli.deleteExchange("wm-app-queue");
        if(f1){
            logger.info("删除Exchange失败");
            return  0;
        }

        rabbitMqCli.deleteQueue("dead.queue.warning.alert",false,paraConsumerBean.isEmptyFlag());
        return 1;
    }

    /**
     * 重置rabbitmq
     */
    public int rabbitReset(){


        listDataExchange=mq.selectAllMq();
        try{
            for(DataExchange dataExchange:listDataExchange){

                if (dataExchange.getType() == 1) {
                    rabbitMqCli.deleteExchange(dataExchange.getExchangeName());
                    rabbitMqCli.deleteExchange("dead.exchange." + dataExchange.getQueueName());
                }
                rabbitMqCli.deleteQueue(dataExchange.getQueueName());
                rabbitMqCli.deleteQueue("dead." + dataExchange.getQueueName());
                rabbitMqCli.deleteQueue("dead.queue.warning.alert." + dataExchange.getQueueName());

            }


        }catch (Exception e){
            logger.error("UpdateMessageDefine失败");
            return 0;
        }

        return 1;

    }



}
