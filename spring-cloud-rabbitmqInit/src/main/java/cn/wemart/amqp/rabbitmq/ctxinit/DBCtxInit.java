package cn.wemart.amqp.rabbitmq.ctxinit;



import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.spring.stat.DruidStatInterceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.aspectj.lang.annotation.Pointcut;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by James on 2017/4/11.
 */
@Configuration
@PropertySource(value = {"file:/opt/appconfig/macys/rabbitmq.properties","file:/opt/appconfig/rabbitmq/jdbc.properties", "file:/opt/appconfig/rabbitmq/http.properties", "file:/opt/appconfig/rabbitmq/monitor.properties"},ignoreResourceNotFound = true )
public class DBCtxInit {

    @Value("${sysinit.jdbc.driver}")
    private String DRIVER_CLASS_NAME;

    @Value("${sysinit.jdbc.url}")
    private String URL;
    @Value("${sysinit.jdbc.username}")
    private String USER_NAME;
    @Value("${sysinit.jdbc.validationQuery}")
    private String VALIDATION_QUERY;
    @Value("${sysinit.jdbc.password}")
    private String PASSWORD;
    @Value("${sysinit.jdbc.initialSize}")
    private int INITIAL_SIZE;
    @Value("${sysinit.jdbc.minIdle}")
    private int MIN_IDLE;
    @Value("${sysinit.jdbc.maxActive}")
    private int MAX_ACTIVE;
    @Value("${sysinit.defaultReadOnly}")
    private boolean DEFAULT_READONLY;
    @Value("${sysinit.defaultAutoCommit}")
    private boolean DEFAULT_AUTOCOMMIT;
    @Value("${sysinit.jdbc.maxWait}")
    private long MAX_WAIT;
    @Value("${sysinit.jdbc.timeBetweenEvictionRunsMillis}")
    private long TIME_BETWEEN_EVICTION_RUNS_MILLIS;
    @Value("${sysinit.jdbc.minEvictableIdleTimeMillis}")
    private long MIN_EVICTABLE_IDLE_TIME_MILLIS;
    @Value("${sysinit.jdbc.poolPreparedStatements}")
    private boolean POOL_PREPARED_STATEMENTS;
    @Value("${sysinit.jdbc.maxOpenPreparedStatements}")
    private int MAX_OPEN_PREPARED_STATEMENTS;

    @Value("${sysinit.jdbc.filters}")
    private String FILTERS;

    @Value("${sysinit.jdbc.testWhileIdle}")
    private boolean TEST_WHILE_IDLE;
    @Value("${sysinit.jdbc.testOnBorrow}")
    private boolean TEST_ON_BORROW;
    @Value("${sysinit.jdbc.testOnReturn}")
    private boolean TEST_ON_RETURN;

    @Value("${sysinit.druid.stat.mergeSql}")
    private String MERGE_SQL;

    @Value("${sysinit.druid.stat.slowSqlMillis}")
    private String SLOW_SQL_MILLIS;

    @Value("${sysinit.useglobaldatasourcestat}")
    private boolean USE_GLOBAL_DATA_SOURCE_STAT;

    @Value("${sysinit.mybatis.typeAliasesPackage}")
    private String MYBATIS_TYPE_ALIASES_PACKAGE;

    @Value("${sysinit.mybatis.mapperLocation}")
    private String MAPPER_LOCATION;




    @Bean
    DataSource dataSource() throws SQLException
    {
        DruidDataSource druidDataSource=new DruidDataSource();
        druidDataSource.setDriverClassName(DRIVER_CLASS_NAME);
        druidDataSource.setUrl(URL);
        druidDataSource.setUsername(USER_NAME);
        druidDataSource.setValidationQuery(VALIDATION_QUERY);
        druidDataSource.setPassword(PASSWORD);

        //初始化大小，最小，最大
        druidDataSource.setInitialSize(INITIAL_SIZE);
        druidDataSource.setMinIdle(MIN_IDLE);
        druidDataSource.setMaxActive(MAX_ACTIVE);

        //是否是只读数据
        druidDataSource.setDefaultReadOnly(DEFAULT_READONLY);
        //打开自动提交事务
        druidDataSource.setDefaultAutoCommit(DEFAULT_AUTOCOMMIT);

        //配置获取连接等待超时的时间
        druidDataSource.setMaxWait(MAX_WAIT);

        //失效检查线程运行时间间隔,配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒,如果小于等于0，不会启动检查线程.
        druidDataSource.setTimeBetweenEvictionRunsMillis(TIME_BETWEEN_EVICTION_RUNS_MILLIS);

        //进行连接空闲时间判断,配置一个连接在池中最小生存的时间，单位是毫秒
        druidDataSource.setMinEvictableIdleTimeMillis(MIN_EVICTABLE_IDLE_TIME_MILLIS);

        //打开PSCache，并且指定每个连接上PSCache的大小.注意：如果用Oracle，则把poolPreparedStatements配置为true，mysql可以配置为false。分库分表较多的数据库，建议配置为false。
        druidDataSource.setPoolPreparedStatements(POOL_PREPARED_STATEMENTS);
        druidDataSource.setMaxOpenPreparedStatements(MAX_OPEN_PREPARED_STATEMENTS);

        //配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于开启druid sql防火墙
        druidDataSource.setFilters(FILTERS);

        //数据库连接因为某种原因断掉后，再从连接池中取得连接又不进行验证，这时取得的连接实际已经时无效的数据库连接了,以下三个属性的意义是取得对象、返回对象和空闲时是否进行验证，检查对象是否有效,false即不验证
        //空闲时是否进行验证，检查对象是否有效.失效连接主要通过testWhileIdle保证，如果获取到了不可用的数据库连接，一般由应用处理异常。
        druidDataSource.setTestWhileIdle(TEST_WHILE_IDLE);

        //生产环境打开会耗费性能
        //取得对象时是否进行验证，检查对象是否有效
        druidDataSource.setTestOnBorrow(TEST_ON_BORROW);

        //返回对象时是否进行验证，检查对象是否有效
        druidDataSource.setTestOnReturn(TEST_ON_RETURN);

        //通过connectProperties属性来打开mergeSql功能；慢SQL记录
        Properties properties=new Properties();
        properties.setProperty("druid.stat.mergeSql",MERGE_SQL);
        properties.setProperty("druid.stat.slowSqlMillis",SLOW_SQL_MILLIS);
        druidDataSource.setConnectProperties(properties);

        //合并多个DruidDataSource的监控数据
        druidDataSource.setUseGlobalDataSourceStat(USE_GLOBAL_DATA_SOURCE_STAT);

        return druidDataSource;
    }

    @Bean("sqlSessionFactory")
    SqlSessionFactory sqlSessionFactoryBean() throws SQLException {
        SqlSessionFactoryBean sqlSessionFactoryBean=new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource());
        //mybatis pojo对象包路径
        sqlSessionFactoryBean.setTypeAliasesPackage(MYBATIS_TYPE_ALIASES_PACKAGE);

        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath:mybatis-mapping/*.xml"));
            return sqlSessionFactoryBean.getObject();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }


    }

    @Bean
    SqlSessionTemplate sqlSessionTemplate() throws Exception {
        SqlSessionTemplate sqlSessionTemplate=new SqlSessionTemplate(sqlSessionFactoryBean());
        return sqlSessionTemplate;

    }
    @Bean
    @Pointcut
    DruidStatInterceptor druidStatInterceptor(){
        DruidStatInterceptor druidStatInterceptor=new DruidStatInterceptor();
        return druidStatInterceptor;
    }



}
