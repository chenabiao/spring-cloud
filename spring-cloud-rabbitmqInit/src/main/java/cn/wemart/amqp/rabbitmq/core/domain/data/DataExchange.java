package cn.wemart.amqp.rabbitmq.core.domain.data;

/**
 * Created by huangkai on 17-6-20.
 */
public class DataExchange {

    private String exchangeName;
    private String queueName;
    private Byte type = 0;

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public DataExchange()
    {

        exchangeName = "";

        queueName = "";

    }
}
