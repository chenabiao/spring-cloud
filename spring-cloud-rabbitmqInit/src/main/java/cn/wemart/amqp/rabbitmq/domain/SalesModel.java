package cn.wemart.amqp.rabbitmq.domain;

/**
 * Created by chenbiao on 2017/7/10.
 */
public class SalesModel {
    private String skuId;

    private Integer sales;

    public SalesModel() {
    }

    public SalesModel(String skuId, Integer sales, Integer stock) {
        this.skuId = skuId;
        this.sales = sales;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    @Override
    public String toString() {
        return "SalesModel{" +
                "skuId='" + skuId + '\'' +
                ", sales=" + sales +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SalesModel that = (SalesModel) o;

        if (skuId != null ? !skuId.equals(that.skuId) : that.skuId != null) return false;
        return sales != null ? sales.equals(that.sales) : that.sales == null;
    }

    @Override
    public int hashCode() {
        int result = skuId != null ? skuId.hashCode() : 0;
        result = 31 * result + (sales != null ? sales.hashCode() : 0);
        return result;
    }

}
