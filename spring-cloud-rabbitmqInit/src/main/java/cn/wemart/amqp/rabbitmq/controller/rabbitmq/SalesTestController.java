package cn.wemart.amqp.rabbitmq.controller.rabbitmq;

import cn.wemart.amqp.rabbitmq.domain.SalesModel;
import com.alibaba.fastjson.JSON;
import org.apache.http.HttpResponse;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by chenbiao on 2017/7/10.
 */
@RestController
public class SalesTestController {
    @RequestMapping(value = "/salestest",method = RequestMethod.POST)
    public @ResponseBody Map createMessage(@RequestBody Map<String,List<SalesModel>> salesMap) {
        List<SalesModel> salesModels = salesMap.get("sku_list");
        Map<String,Object> map = new HashMap<>();
        if (salesModels.size()>0){
            map.put("code",0);
        }else{
            map.put("code",1);
        }
        System.out.println("sales收到消息了");
        System.out.println(JSON.toJSON(map).toString());
        return map;
    }
}
