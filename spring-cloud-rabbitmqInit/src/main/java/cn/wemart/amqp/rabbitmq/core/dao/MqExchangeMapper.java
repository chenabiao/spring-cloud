package cn.wemart.amqp.rabbitmq.core.dao;

import cn.wemart.amqp.rabbitmq.core.domain.MqExchange;
import org.springframework.stereotype.Component;

@Component
public interface MqExchangeMapper {
    int deleteByPrimaryKey(Integer exchangeId);

    int insert(MqExchange record);

    int insertSelective(MqExchange record);

    MqExchange selectByPrimaryKey(Integer exchangeId);

    int updateByPrimaryKeySelective(MqExchange record);

    int updateByPrimaryKey(MqExchange record);

}