package cn.wemart.amqp.rabbitmq.controller.rabbitmq;


import cn.wemart.amqp.rabbitmq.core.init.CreateMessageDefine;
import cn.wemart.amqp.rabbitmq.core.init.UpdateMessageDefine;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by chenbiao on 17-6-16.
 */
@RequestMapping("/rabbitmq")
@RestController
public class MgntController {


    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(MgntController.class);

    @Autowired
    private CreateMessageDefine createMessageDefine;

    @RequestMapping("/mgnt")
    public String createMessage() {

        try {
            createMessageDefine.createMessage();
        } catch (IOException e) {
            logger.error("mq初始化失败[{}]",e.getMessage(),e);
            return "error";
        }

        return  "success";
    }


}
