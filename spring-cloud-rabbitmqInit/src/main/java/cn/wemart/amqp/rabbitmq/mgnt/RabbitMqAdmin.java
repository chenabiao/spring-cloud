package cn.wemart.amqp.rabbitmq.mgnt;


import org.springframework.amqp.core.*;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.*;


/**
 * Created by huangkai on 17-6-20.
 */

@Component
public class RabbitMqAdmin {

    @Autowired
    private AmqpAdmin amqpAdmin;

    /**
     *
     * @param exchangeName
     * 名称
     *
     * @param type
     * 类型 有Direct exchange，Fanout exchange，Topic exchange和Headers exchange
     *
     * @param isDurable
     * 持久化标志，如果为true，则表明此exchange是持久化的
     *
     * @param isAutoDelete
     * 删除标志，当所有队列在完成使用此exchange时，是否删除
     *
     */
    public Exchange getExchange(String exchangeName,String type,boolean isDurable,boolean isAutoDelete){

        Exchange exchange=new ExchangeImpl() {
            @Override
            public String getName() {
                return exchangeName;
            }

            @Override
            public String getType() {
                return type;
            }

            @Override
            public boolean isDurable() {
                return isDurable;
            }

            @Override
            public boolean isAutoDelete() {
                return isAutoDelete;
            }

            @Override
            public Map<String, Object> getArguments() {
                return null;
            }

            @Override
            public boolean isDelayed() {
                return false;
            }

            @Override
            public boolean isInternal() {
                return false;
            }

            @Override
            public boolean shouldDeclare() {
                return false;
            }

            @Override
            public Collection<?> getDeclaringAdmins() {
                return null;
            }

            @Override
            public boolean isIgnoreDeclarationExceptions() {
                return false;
            }
        };

        return exchange;
    }

    /**
     * @param isExclusive
     * 是否独占，只能被一个consumer的conn占用
     * @param isDead
     * 是否是DLX(死信队列)
     *
     */
    public Queue getQueue(String queueName,boolean isDurable,boolean isExclusive,boolean isAutoDelete,boolean isDead){
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("x-ha-policy", "all");// HA 一种高可用方案
        if(!isDead)
            param.put("x-dead-letter-exchange", "dead.exchange."+queueName);//DLX 死信队列
        Queue queue=new Queue(queueName,isDurable,isExclusive,isAutoDelete,param);
        return queue;
    }


    public void declareQueue(Queue queue) throws IOException
    {

        amqpAdmin.declareQueue(queue);
    }

    public void declareExchange(Exchange exchange) throws IOException
    {

        amqpAdmin.declareExchange(exchange);

    }

    /**
     * @param routingKey
     * 一种绑定键
     * Fanout exchange方式可设routingKey为“”
     *
     */
    public void declareBinding(Queue queue,Exchange exchange,String routingKey)throws IOException{

        amqpAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey).noargs());

    }


    /**
     *
     * @param queueName
     * @param unUseFlag 没有使用标志 true 没有使用  false 已使用
     * @param emptyFlag 是否为空标志 true 为空  false 不为空
     */
    public void deleteQueue(String queueName,boolean unUseFlag,boolean emptyFlag){
        amqpAdmin.deleteQueue(queueName,unUseFlag,emptyFlag);
    }

    public boolean deleteExchange(String exchangeName){
        return amqpAdmin.deleteExchange(exchangeName);
    }

    public boolean deleteQueue(String queueName){
        return amqpAdmin.deleteQueue(queueName);
    }



    public void declareExcQueue(String exchangeName,String queueName,boolean ifHasExchange)throws IOException{


        String type="";
        String routingKey="";
        if(ifHasExchange)
            type="fanout";

        else {
            type="direct";
            routingKey=queueName;
        }



        Exchange exchange=getExchange(exchangeName,"fanout",true,false);
        Queue queue=getQueue(queueName,true,false,false,false);

        declareExchange(exchange);
        declareQueue(queue);
        declareBinding(queue,exchange,routingKey);

        declareDeadQueue(queueName);
    }



    /**
     * 所有queue都有一个对应的deadQueue
     */
    public void declareDeadQueue(String queueName)throws IOException{
        Exchange exchange=getExchange("dead.exchange."+queueName,"fanout",true,false);
        Queue queue=getQueue("dead."+queueName,true,false,false,true);
        Queue queueWarn=getQueue("dead.queue.warning.alert."+queueName,true,false,false,true);

        declareExchange(exchange);
        declareQueue(queue);
        declareQueue(queueWarn);
        declareBinding(queue,exchange,"");
        declareBinding(queueWarn,exchange,"");

    }



    private class ExchangeImpl implements Exchange{

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getType() {
            return null;
        }

        @Override
        public boolean isDurable() {
            return false;
        }

        @Override
        public boolean isAutoDelete() {
            return false;
        }

        @Override
        public Map<String, Object> getArguments() {
            return null;
        }

        @Override
        public boolean isDelayed() {
            return false;
        }

        @Override
        public boolean isInternal() {
            return false;
        }

        @Override
        public boolean shouldDeclare() {
            return false;
        }

        @Override
        public Collection<?> getDeclaringAdmins() {
            return null;
        }

        @Override
        public boolean isIgnoreDeclarationExceptions() {
            return false;
        }
    }



}
