package cn.wemart.amqp.rabbitmq.core.init;

import cn.wemart.amqp.rabbitmq.core.domain.data.DataExchange;
import cn.wemart.amqp.rabbitmq.core.dao.MqQueueMapper;
import cn.wemart.amqp.rabbitmq.mgnt.RabbitMqAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by huangkai on 17-6-20.
 */

@Component
public class CreateMessageDefine {

    private final static Logger logger = LoggerFactory.getLogger(CreateMessageDefine.class);
    private List<DataExchange> listDataExchange;


    @Autowired
    private  MqQueueMapper mq;

    @Autowired
    private RabbitMqAdmin rabbitMqCli;

    public int createMessage() throws IOException {

        logger.info("creating message!");



        listDataExchange=mq.selectAllMq();
        if(listDataExchange.size()==0||listDataExchange==null){
            logger.error("DataExchange获取失败");
            return 0;
        }


        for(DataExchange dataExchange:listDataExchange){

            String exchangeName="";
            boolean ifHasExchange=false;

            //dataExchange.getType() 表示是否有Exchange与之绑定，1 有，0 没有
            if(dataExchange.getType()==1){
                ifHasExchange=true;
                exchangeName=dataExchange.getExchangeName();
            }else{
                ifHasExchange=false;
                exchangeName="wm-app-queue";//所有没有exchange的Queue都与“wm-app-queue”绑定
            }

            rabbitMqCli.declareExcQueue(exchangeName, dataExchange.getQueueName(),ifHasExchange);


        }


        return 1;
    }
}
