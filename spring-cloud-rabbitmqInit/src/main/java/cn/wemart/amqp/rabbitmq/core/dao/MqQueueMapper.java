package cn.wemart.amqp.rabbitmq.core.dao;

import cn.wemart.amqp.rabbitmq.core.domain.MqQueue;
import cn.wemart.amqp.rabbitmq.core.domain.data.DataExchange;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MqQueueMapper {
    int deleteByPrimaryKey(@Param("queueId") Integer queueId, @Param("exchangeId") String exchangeId);

    int insert(MqQueue record);

    int insertSelective(MqQueue record);

    MqQueue selectByPrimaryKey(@Param("queueId") Integer queueId, @Param("exchangeId") String exchangeId);

    int updateByPrimaryKeySelective(MqQueue record);

    int updateByPrimaryKey(MqQueue record);

    List<DataExchange> selectAllMq();

}