package cn.wemart.amqp.rabbitmq.core.domain.para;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangkai on 17-6-20.
 */
public class ParaConsumerBean {
    //队列类型
    private Byte queueType;
    //队列名字数组
    private List<String> queueNameList;
    //交换机名数组
    private List<String> exchangeNameList;
    //没有使用标志 true 没有使用  false 已使用
    private Boolean unUseFlag;
    //是否为空标志 true 为空  false 不为空
    private Boolean emptyFlag;


    public ParaConsumerBean(){

        queueType = (byte)0;
        queueNameList = new ArrayList<>();
        exchangeNameList = new ArrayList<>();
        unUseFlag = false;
        emptyFlag = false;

    }
    public Byte getQueueType() {
        return queueType;
    }

    public void setQueueType(Byte queueType) {

        this.queueType = queueType == null ? (byte)0 : queueType;
    }

    public List<String> getQueueNameList() {
        return queueNameList;
    }

    public void setQueueNameList(List<String> queueNameList) {
        if(queueNameList != null){
            this.queueNameList = queueNameList;
        }
    }

    public List<String> getExchangeNameList() {
        return exchangeNameList;
    }

    public void setExchangeNameList(List<String> exchangeNameList) {
        if(exchangeNameList != null){
            this.exchangeNameList = exchangeNameList;
        }
    }

    public Boolean isUnUseFlag() {
        return unUseFlag;
    }

    public void setUnUseFlag(Boolean unUseFlag) {
        this.unUseFlag = unUseFlag == null ? false : unUseFlag;
    }

    public Boolean isEmptyFlag() {
        return emptyFlag;
    }

    public void setEmptyFlag(Boolean emptyFlag) {
        this.emptyFlag = emptyFlag == null ? false : emptyFlag;
    }
}
