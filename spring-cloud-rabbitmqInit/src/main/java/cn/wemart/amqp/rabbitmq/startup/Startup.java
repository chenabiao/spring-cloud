package cn.wemart.amqp.rabbitmq.startup;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("cn.wemart.amqp.rabbitmq.core.dao")
@ComponentScan("cn.wemart.amqp.rabbitmq")
public class Startup {


	public static void main(String[] args) {


		SpringApplication.run(Startup.class, args);
	}


}
