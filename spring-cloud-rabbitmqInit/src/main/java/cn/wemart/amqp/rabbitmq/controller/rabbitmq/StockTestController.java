package cn.wemart.amqp.rabbitmq.controller.rabbitmq;

import cn.wemart.amqp.rabbitmq.domain.StockModel;
import com.alibaba.fastjson.JSON;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by chenbiao on 2017/7/10.
 */
@RestController
public class StockTestController {
    @RequestMapping(value = "/stocktest",method = RequestMethod.POST)
    public @ResponseBody Map createMessage(@RequestBody Map<String,List<StockModel>> stockMap) {
        List<StockModel> stockModels = stockMap.get("sku_list");
        Map<String,Object> map = new HashMap<>();
        if (stockModels.size()>0){
            map.put("code",0);
        }else{
            map.put("code",1);
        }
        System.out.println("stock收到消息了");
        System.out.println(JSON.toJSON(map).toString());
        return map;
    }
}
