package cn.wemart.amqp.rabbitmq.domain;

/**
 * Created by chenbiao on 2017/7/10.
 */
public class StockModel {
    private String skuId;

    private Integer stock;

    public StockModel() {
    }

    public StockModel(String skuId, Integer stock) {
        this.skuId = skuId;
        this.stock = stock;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "StockModel{" +
                "skuId='" + skuId + '\'' +
                ", stock=" + stock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StockModel that = (StockModel) o;

        if (skuId != null ? !skuId.equals(that.skuId) : that.skuId != null) return false;
        return stock != null ? stock.equals(that.stock) : that.stock == null;
    }

    @Override
    public int hashCode() {
        int result = skuId != null ? skuId.hashCode() : 0;
        result = 31 * result + (stock != null ? stock.hashCode() : 0);
        return result;
    }

}
