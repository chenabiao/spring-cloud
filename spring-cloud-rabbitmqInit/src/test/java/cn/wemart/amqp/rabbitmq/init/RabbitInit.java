package cn.wemart.amqp.rabbitmq.init;

import cn.wemart.amqp.rabbitmq.core.init.CreateMessageDefine;
import cn.wemart.amqp.rabbitmq.core.init.UpdateMessageDefine;
import cn.wemart.amqp.rabbitmq.startup.Startup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * Created by huangkai on 17-6-28.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Startup.class)
public class RabbitInit {

    @Autowired
    private CreateMessageDefine cd;

    @Autowired
    private UpdateMessageDefine ud;

    @Autowired
    private RabbitTemplate rabbitTemplatete;



    @Test
    public void createMessage() throws IOException {

        cd.createMessage();
    }

    @Test
    //清空所有已有用户队列和exchange1
    public void resetRabbitmq(){
        int result=ud.rabbitReset();
        System.out.println(result==1?"重置rabbitmq成功":"重置rabbitmq失败");
    }
}
